package Exception;
import java.lang.Exception;

public class NotCException extends Exception{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public NotCException(String message) {
        super(message);
    }
}