import java.util.Scanner;
import java.util.Queue;
import java.util.LinkedList;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.Exception;
import Exception.NotCException;

public class Lexico{
    static String [] tokens;
    public static void main(String[] args) {
        File text;
        Scanner sc;
        Queue<String> q = new LinkedList<String>();
        try{
            String s="",str="";
            sc = new Scanner(new File("input.c"));
            while(sc.hasNextLine()){
                s = sc.nextLine();
                str = str.concat(s);
            }
            sc.close();

            tokens = str.split("(/\\*.*?\\*/)");
            str = "";
            for (int i = 0; i < tokens.length; i++) {
                str = str.concat(tokens[i]);
            }
            tokens = str.split("(//.*?\\s)");
            str = "";
            for (int i = 0; i < tokens.length; i++) {
                str = str.concat(tokens[i]);
            }
            tokens = str.split("\\s+");
            str = "";
            for (int i = 0; i < tokens.length; i++) {
                str = str.concat(tokens[i]);
            }
            //retira as aspas e o que estiver dentro.
            String strings[] = str.split("\"(.*?)\"");
            String string="";
            for (int i = 0; i < strings.length; i++) {
                string = string.concat(strings[i]);
            }
            verify(string);//Verifica se é ASCII se não for ele lança um erro.

            for (int i = 0; i < tokens.length; i++) {
                System.out.println(tokens[i]);
                q.add(tokens[i]);
            }
        }
        catch(NotCException nc){
            nc.printStackTrace();
        }
        catch(Exception e){
            System.out.println("Something go wrong !");
        }
    }
    public static void verify(String str) throws NotCException{
        if(!str.matches("[\\p{ASCII}]*?")) //O alfabeto de C é ASCII.
            throw new NotCException("Not a C compatible characters set.");
    }
}
